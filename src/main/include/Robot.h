/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <frc/TimedRobot.h>
#include "networktables/NetworkTable.h"
#include "networktables/NetworkTableInstance.h"
#include "frc/Victor.h"
#include "frc/WPILib.h"
#include "ctre/Phoenix.h"
#include <stdio.h>
#include <iostream>
#include <cmath>
class Robot : public frc::TimedRobot {
 public:
  void RobotInit() override;

  void AutonomousInit() override;
  void AutonomousPeriodic() override;

  void TeleopInit() override;
  void TeleopPeriodic() override;

  void TestInit() override;
  void TestPeriodic() override;

  void Tdrive(double left, double right);
  void DriveForwards(double speed);

  void initTable();
  double getLeftTravel();
  double getRightTravel();

  bool findTarget();
  double GetTargetDistance(double vert);

  bool lightOn = true;
  bool lightButtonHeld = false;

  double AutoEncoderStartRight = 0;
  double AutoEncoderStartLeft = 0;

  bool foundTarget = false;

  nt::NetworkTableEntry e_tx;
  nt::NetworkTableEntry e_ty;
  nt::NetworkTableEntry e_ts;
  nt::NetworkTableEntry e_tv;
  nt::NetworkTableEntry e_tVert;

  nt::NetworkTableEntry light;

  nt::NetworkTableEntry rightEnc;
  nt::NetworkTableEntry leftEnc;
  
  nt::NetworkTableEntry autoDelta;
  nt::NetworkTableEntry reportedGyro;
  nt::NetworkTableEntry detectedDistance;

  nt::NetworkTableEntry autoRotSpeedSetting;


  frc::Victor RightMotor { 9 };
	frc::Victor LeftMotor { 8 };

  ctre::phoenix::motorcontrol::can::TalonSRX RightTalon { 1 };
	ctre::phoenix::motorcontrol::can::TalonSRX LeftTalon { 2 };

	// frc::Compressor comp { 0 };
	// frc::DoubleSolenoid shifter1 { 0, 1 };
	// frc::DoubleSolenoid shifter2 {2,3};
  
	frc::Joystick stick { 0 };

  frc::ADXRS450_Gyro gyro {};

  double leftTicks;
  double rightTicks;

  double beginLeftEnc;
  double beginRightEnc;

  double d_leftTicks;
  double d_rightTicks;

  double rightTPF = 9132;
  double leftTPF = -9132;

  bool near_threshold = false;
  //double rightTPF = 9132;
  //double leftTPF = -8747.9;
};
