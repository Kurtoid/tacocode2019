/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

void Robot::RobotInit() {
    // gyro.Reset();
    // gyro.Calibrate();
    // std::cout<<"Calibrated gyro"<<std::endl;
}
void Robot::initTable(){
    auto inst = nt::NetworkTableInstance::GetDefault();
    auto table = inst.GetTable("SmartDashboard");
    rightEnc = table->GetEntry("right");
    leftEnc = table->GetEntry("left");
    autoDelta = table->GetEntry("autoDelta");
    reportedGyro = table->GetEntry("gyro");
    autoRotSpeedSetting = table->GetEntry("Auto Rotation Speed");
    detectedDistance = table->GetEntry("Target Dist");

    table = inst.GetTable("limelight");
    e_tx = table->GetEntry("tx");
    e_ty = table->GetEntry("ty");
    e_ts = table->GetEntry("ts");
    e_tv = table->GetEntry("tv");
    e_tVert = table->GetEntry("tvert");

    light = table->GetEntry("ledMode");


}
void Robot::AutonomousInit() {
    initTable();
    beginRightEnc = getRightTravel();
    beginLeftEnc = getLeftTravel();

    rightTicks = getRightTravel() - beginRightEnc;
    leftTicks = getLeftTravel() - beginLeftEnc;

    AutoEncoderStartRight = rightTicks;
    AutoEncoderStartLeft = leftTicks;


}
void Robot::AutonomousPeriodic() {
    rightTicks = getRightTravel() - beginRightEnc;
    leftTicks = getLeftTravel() - beginLeftEnc;
    reportedGyro.SetDouble(gyro.GetAngle());
    DriveForwards(0.25);
}

void Robot::TeleopInit() {
    initTable();
    beginRightEnc = getRightTravel();
    beginLeftEnc = getLeftTravel();

}
void Robot::TeleopPeriodic() {
    double angle = e_ts.GetDouble(0.0);
    double xPos = e_tx.GetDouble(0.0);
    double yPos = e_ty.GetDouble(0.0);
    
    d_rightTicks = getRightTravel() - rightTicks;
    d_leftTicks = getLeftTravel() - leftTicks;

    rightTicks = getRightTravel() - beginRightEnc;
    leftTicks = getLeftTravel() - beginLeftEnc;
    rightTicks /= leftTPF;
    leftTicks /= rightTPF;
    rightEnc.SetDouble(rightTicks);
    leftEnc.SetDouble(leftTicks);

    Tdrive(-stick.GetRawAxis(5), -stick.GetRawAxis(1));
    if(stick.GetRawButton(1)){
        if(!lightButtonHeld){
            lightButtonHeld = true;
            lightOn=!lightOn;
            AutonomousInit();
        }
        DriveForwards(-stick.GetRawAxis(1));
    }else{
        lightButtonHeld = false;
    }

    if(lightOn){
        light.SetDouble(3);
    }else{
        light.SetDouble(1);
    }
    reportedGyro.SetDouble(gyro.GetAngle());
    if(stick.GetRawButton(2)){
        findTarget();
    }else{
        foundTarget = false;
        near_threshold = false;
    }
    detectedDistance.SetDouble(GetTargetDistance(e_tVert.GetDouble(0)));
}
void Robot::Tdrive(double Right, double Left) {
    // RightMotor.SetSpeed(-Right);
    // LeftMotor.SetSpeed(Left);
    RightTalon.Set(ControlMode::PercentOutput, -Right);
    LeftTalon.Set(ControlMode::PercentOutput, Left);
}

void Robot::DriveForwards(double speed){
    double EncoderDelta = (rightTicks - AutoEncoderStartRight) - (leftTicks - AutoEncoderStartLeft);
    autoDelta.SetDouble(EncoderDelta);
    EncoderDelta *= 2;
    Tdrive(speed - EncoderDelta,speed + EncoderDelta);

}

bool Robot::findTarget(){
    lightOn = true;
    double error = e_tx.GetDouble(0.0) / (27*2); // -1 to 1
    
    double speed = autoRotSpeedSetting.GetDouble(0.5);
    autoDelta.SetDouble(error);
    double speed_falloff = 24;
    if((GetTargetDistance(e_tVert.GetDouble(0.0))/speed_falloff)<1){
        speed*=(GetTargetDistance(e_tVert.GetDouble(0.0))/speed_falloff);
        if(GetTargetDistance(e_tVert.GetDouble(0.0)<30)){
            near_threshold = true;
        }
    }
    if(e_tv.GetDouble(0.0) == 1.0 && GetTargetDistance(e_tVert.GetDouble(0.0))>30){ // if found
        Tdrive(speed - error, speed + error);
    }else if (!near_threshold){
        DriveForwards(speed);
    }else{
        DriveForwards(0);
    }
}

double Robot::getLeftTravel(){
    return LeftTalon.GetSelectedSensorPosition()/leftTPF;
}

double Robot::getRightTravel(){
    return RightTalon.GetSelectedSensorPosition()/rightTPF;
}
double Robot::GetTargetDistance(double vert){
    return 282.44*std::pow(M_E, -0.07*vert);
}
void Robot::TestInit() {}
void Robot::TestPeriodic() {}

#ifndef RUNNING_FRC_TESTS
int main() { return frc::StartRobot<Robot>(); }
#endif
